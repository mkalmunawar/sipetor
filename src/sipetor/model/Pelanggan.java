/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package sipetor.model;

/**
 *
 * @author MK Almunawar
 */
public class Pelanggan {
    private String tnkb ;
    private String nama_pelanggan ;
    private String alamat_pelanggan;
    
    public Pelanggan(String tnkb, String nama_pelanggan, String alamat_pelanggan){
        this.tnkb = tnkb;
        this.nama_pelanggan = nama_pelanggan;
        this.alamat_pelanggan = alamat_pelanggan;
    }

    public String getTnkb() {
        return tnkb;
    }

    public void setTnkb(String tnkb) {
        this.tnkb = tnkb;
    }

    public String getNama_pelanggan() {
        return nama_pelanggan;
    }

    public void setNama_pelanggan(String nama_pelangan) {
        this.nama_pelanggan = nama_pelangan;
    }

    public String getAlamat_pelanggan() {
        return alamat_pelanggan;
    }

    public void setAlamat_pelanggan(String alamat_pelanggan) {
        this.alamat_pelanggan = alamat_pelanggan;
    }
    
}
