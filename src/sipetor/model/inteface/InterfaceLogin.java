/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package sipetor.model.inteface;

import java.util.List;
import sipetor.model.Login;

/**
 *
 * @author MK Almunawar
 */
public interface InterfaceLogin {
     
    public boolean insert(Login data);
    public boolean update (Login data);
    public boolean delete(String nik);
    
    public String isCorrect(String username, String password);
    
}
