package sipetor.model.inteface;



import java.util.List;
import sipetor.model.Golongan;

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
/**
 *
 * @author MK Almunawar
 */
public interface InterfaceGolongan {
    public boolean insert(Golongan data);
    public boolean update(Golongan data);
    public boolean delete(byte id_golongan);
    public int generateID();
    
    public List<Golongan> getAllGolongan();
    
    public Golongan getOneGolonganByID(byte id_golongan);
    
    public List<Golongan> getAllGolonganByName(String nama_golongan);
    
}
