/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package sipetor.controller;

import java.util.List;
import sipetor.model.Sparepart;
import sipetor.query.QuerySparepart;

/**
 *
 * @author MK Almunawar
 */
public class ControllerSparepart {
    private QuerySparepart querySparepart = new  QuerySparepart();
    
    public List<Sparepart> getAllData() {
        return querySparepart.getAllSparepart();
    }
    
    public List<Sparepart> getAllSparepartByName(String nama_sparepart) {
        return querySparepart.getAllSparepartByName(nama_sparepart);
    }
    
    public boolean insertSparepart(Sparepart data){
        return querySparepart.insert(data);
    }
    
    public boolean updateSparepart(Sparepart data){
        return querySparepart.update(data);
    }
    
    public boolean deleteSparepart(byte id){
        return querySparepart.delete(id);
    }
    
    public int generateID(){
        return querySparepart.generateID();
    }
 
    
}
