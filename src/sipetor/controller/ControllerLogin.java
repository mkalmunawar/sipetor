/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package sipetor.controller;

import java.util.List;
import sipetor.model.Login;
import sipetor.query.QueryLogin;

/**
 *
 * @author MK Almunawar
 */
public class ControllerLogin {
    private QueryLogin queryLogin = new QueryLogin();
    
    public String isCorrect(String username, String password){
        return queryLogin.isCorrect(username, password);
    }
    
    public boolean insert(Login data){
        return queryLogin.insert(data);
    }
    
    public boolean delete(String nik){
        return queryLogin.delete(nik);
    }
    
}
